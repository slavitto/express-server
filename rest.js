let express = require('express');
let app 	= express();
let methodOverride = require('method-override');
app.use(methodOverride('_method'))

app.post('/api/users/', (req, res) => {
	if (!req.query.name || req.query.name == 'undefined') {
		res.status(400);
		res.send('User name is undefined');
	} else {
		// генерация ID, занесение в базу, с проверкой на ошибку базы
		res.status(201);
		res.send('New user: ' + req.query.name);
	}
});

app.get('/api/users/:id', (req, res) => {
	if (!req.params.id || req.params.id == 'undefined') {
		res.status(400);
		res.send('User ID is undefined');
	} else {
		// получение пользователя из базы по предоставленному id, с проверкой на ошибку базы
		res.status(200);
		res.send('User ' + req.params.id);
	}
});

app.put('/api/users/:id', (req, res) => {
	if (!req.params.id || req.params.id == 'undefined') {
		res.status(400);
		res.send('User ID is undefined');
	} else if (!req.query.name || req.query.name == 'undefined') {
		res.status(400);
		res.send('User name is undefined');
	} else if (!req.query.score || req.query.score == 'undefined') {
		res.status(400);
		res.send('User score is undefined');
	} else {
		// обновление пользователя, с проверкой на ошибку базы
		let r = req.query;
		res.status(202);
		res.send('Name: ' + r.name + 'Score: ' + r.score);
	}
});

app.delete('/api/users/:id', (req, res) => {
	if (!req.params.id || req.params.id == 'undefined') {
		res.status(400);
		res.send('User ID is undefined');
	} else {
		// удаление пользователя из базы по предоставленному id
		res.status(200);
		res.send('User: ' + req.params.id + ' deleted');
	}
});

app.use((req, res, next) => {
  res.status(404).send('Path not found');
});

app.use((err, req, res, next) => {
  res.status(500);
  res.render('error', { error: err });
  next(err);
});

app.listen(3000);