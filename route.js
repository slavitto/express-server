let express = require('express');
let app 	= express();
let bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.status(200).send('Hello Express.js');
});

app.use('/hello', (req, res) => {
	if (req.path==='/') {
		res.status(200).send('Hello stranger');
	} else {
		res.status(200).send('Hello ' + req.path.replace('/', '') + ' !');
	}
});

app.all('/sub/*', (req, res) => {
	res.status(200).send('You requested URI: ' + req.originalUrl);
});

app.post('/post', (req, res) => {
	if (Object.keys(req.body).length !== 0) {
		res.status(200).json(req.body);
	} else {
		res.status(404).send('Not found');
	}
});

app.use((req, res, next) => {
  res.status(404).send('Path not found');
});

app.use((err, req, res, next) => {
  res.status(500);
  res.render('error', { error: err });
  next(err);
});

app.listen(3000);