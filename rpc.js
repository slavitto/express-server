let express = require('express');
let app 	= express();
let methodOverride = require('method-override');
let bodyParser = require('body-parser');

app.use(methodOverride('_method'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/rpc/users/', (req, res) => {
	
	if (req.body.method == 'get') {
		getUser((err, res) => {
			if (err) res.status(400).json({ error: 'bad request' })
			// получение пользователя из базы по предоставленному id, с проверкой на ошибку базы
			res.status(200).json({ user: req.body.name, Score: req.body.score })
		});

	} else if (req.body.method == 'post') {
		postUser((err, res) => {
			if (err) res.status(400).json({ error: 'bad request' })
			// генерация ID, занесение в базу, с проверкой на ошибку базы
			res.status(201).json({ 'New user': req.body.name });
		});

	} else  if (req.body.method == 'put') {
		putUser((err, res) => {
			if (err) res.status(400).json({ error: 'bad request' })
			// обновление пользователя, с проверкой на ошибку базы
			let r = req.body;
			res.status(202).json({ 	Name:  r.name, Score: r.score });
		});

	}  else if (req.body.method == 'delete') {
		deleteUser((err, res) => {
			if (err) res.status(400).json({ error: 'bad request' })
			// удаление пользователя из базы по предоставленному id
			res.status(200).json({ user: req.body.id, state: 'deleted' });
		});

	} else {
		res.status(400).json({ error: 'Undefined method' });
	}
});


app.use((req, res, next) => {
  res.status(404).send('Path not found');
});

app.use((err, req, res, next) => {
  res.status(500);
  res.render('error', { error: err });
  next(err);
});

app.listen(3000);